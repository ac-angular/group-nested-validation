import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, ValidationErrors, ValidatorFn, Validators } from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'group-nested-validation';
  form: FormGroup;

  constructor(private fb: FormBuilder) {
    this.form = this.fb.group({
      firstname: this.fb.control('', Validators.required),
      lastname: this.fb.control('', Validators.required),
      password: this.fb.group({
        password: this.fb.control('', Validators.required),
        confirm: this.fb.control('', Validators.required),
      }, {
        validators: [compareValue('password', 'confirm')]
      })
    });
  }

  ngOnInit() {}

  send() {
    console.log(`%c Habilitado: ${this.form.value.firstname}, ${this.form.value.lastname}, ${this.form.value.password.password}, ${this.form.value.password.confirm}`, 'background: green; color: white; display: block;');
  }
}


export function compareValue(controlName1: string, controlName2:string) {
  return (formGroup: FormGroup):ValidationErrors | null => {
    const value1 = formGroup.controls[controlName1].value;
    const value2 = formGroup.controls[controlName2].value;
    console.log(value1, value2);
    if (!value1 || !value2) {
        return null;
    }
    return value1 !== value2 ? {dismatch:true}: null;
  }
}